"""
a python scrit to be able to run hydro2mqtt directly from python on any OS instead of using docker.
"""
import asyncio
import argparse
import sys
import os
import logging

from hydroqc2mqtt import daemon
from hydroqc2mqtt.contract_device import HydroqcContractDevice

"""
TODO
-handle multiple contract in some way(maybe a folder with a config file for each contract or something)
"""
_SYNC_FREQUENCY = 600
_UNREGISTER_ON_STOP = False

class py_hydro2mqtt(daemon.Hydroqc2Mqtt):
    """
    class that will handle the deamon
    change is that it will take command line argument
    """

    def __init__(self):
        """
        initialize the object"
        """
        self.config = {'sync_frequency': _SYNC_FREQUENCY,\
             'unregister_on_stop': _UNREGISTER_ON_STOP,\
                  'contracts': []}
        config = self.get_args()#get config from args
        self.sync_frequency = _SYNC_FREQUENCY
        self.unregister_on_stop = _UNREGISTER_ON_STOP
        self.config['contracts'] = [config]  
        super().__init__()       

    def read_config(self):

        # Override hydroquebec username and password from env var if exists
        self.config.setdefault("contracts", [])
        if self.config["contracts"] is None:
            self.config["contracts"] = []

        # Handle contracts
        for contract_config in self.config["contracts"]:
            contract = HydroqcContractDevice(
                contract_config["name"], self.logger, contract_config
            )
            contract.add_entities()
            self.contracts.append(contract)        

    def read_base_config(self):
        self.mqtt_username = self.mqtt_username
        self.mqtt_password = self.mqtt_password
        self.mqtt_port = self.mqtt_port

        self.mqtt_discovery_root_topic = 'homeassistant'
        """os.environ.get(
            "MQTT_DISCOVERY_ROOT_TOPIC", os.environ.get("ROOT_TOPIC", "homeassistant")
        )"""
        self.mqtt_data_root_topic = 'homeassistant'
        """os.environ.get(
            "MQTT_DATA_ROOT_TOPIC", "homeassistant"
        )"""
        self._loglevel = os.environ.get("LOG_LEVEL", "INFO").upper()
        self.logger.setLevel(getattr(logging, self._loglevel))



    def get_args(self):
        """
        get argument from command line

        return dict of argument
        """

        #Parsing the cli arguments
        parser = argparse.ArgumentParser(description="hydroqc2mqtt deamon")
        parser.add_argument('MQTT_HOST', help="your mqtt server")
        parser.add_argument('HQ2M_CONTRACTS_0_NAME', help="Name to identify your contract, will be the mqtt topic")
        parser.add_argument('HQ2M_CONTRACTS_0_USERNAME', help="Your Hydro-Quebec account username")
        parser.add_argument('HQ2M_CONTRACTS_0_PASSWORD', help="Your Hydro-Quebec account password")
        parser.add_argument('HQ2M_CONTRACTS_0_CUSTOMER', help="Your Hydro Quebec customer number(invoice number)")#number only
        parser.add_argument('HQ2M_CONTRACTS_0_ACCOUNT', help="Your Hydro-Quebec account number(from invoice)")#number only
        parser.add_argument('HQ2M_CONTRACTS_0_CONTRACT', help="Your Hydro-Quebec contract number(from invoice)")#must be 9 digit, 0 will be added automatically
        parser.add_argument('-u', '--MQTT_USERNAME', help="MQTT username, leave empty if none", default='')
        parser.add_argument('-p', '--MQTT_PASSWORD', help="MQTT password, leave empty if none", default='')
        parser.add_argument('-P', '--MQTT_PORT', help="MQTT server port, default: 1883", default=1883)

        args = parser.parse_args()

        #validating the argument

        #HQ2M_CONTRACTS_0_CONTRACT

        self.is_number_only(args.HQ2M_CONTRACTS_0_CONTRACT,f'{args.HQ2M_CONTRACTS_0_CONTRACT=}'.split("=")[0].split(".")[1], parser)#test if contact is number only
        
        while len(args.HQ2M_CONTRACTS_0_CONTRACT) < 9:#fill with 0 before the contract number to get 9 character
            args.HQ2M_CONTRACTS_0_CONTRACT = ('0{0}'.format(args.HQ2M_CONTRACTS_0_CONTRACT))

        #HQ2M_CONTRACTS_0_CUSTOMER
        self.is_number_only(args.HQ2M_CONTRACTS_0_CUSTOMER,f'{args.HQ2M_CONTRACTS_0_CUSTOMER=}'.split("=")[0].split(".")[1], parser)

        #HQ2M_CONTRACTS_0_ACCOUNT
        self.is_number_only(args.HQ2M_CONTRACTS_0_ACCOUNT,f'{args.HQ2M_CONTRACTS_0_ACCOUNT=}'.split("=")[0].split(".")[1], parser)     

        #MQTT_PORT
        self.is_number_only(args.MQTT_PORT,f'{args.MQTT_PORT=}'.split("=")[0].split(".")[1], parser)
        
        dict = {'name': args.HQ2M_CONTRACTS_0_NAME, 'username': args.HQ2M_CONTRACTS_0_USERNAME, 'password': args.HQ2M_CONTRACTS_0_PASSWORD, 'customer': args.HQ2M_CONTRACTS_0_CUSTOMER, 'account': args.HQ2M_CONTRACTS_0_ACCOUNT, 'contract': args.HQ2M_CONTRACTS_0_CONTRACT, 'verify_ssl': False}

        #Assign mqtt var to self
        self.mqtt_username = args.MQTT_USERNAME
        self.mqtt_password = args.MQTT_PASSWORD
        self.mqtt_port = args.MQTT_PORT
        self.mqtt_host = args.MQTT_HOST

        return dict
    
    def is_number_only(self, args, args_name, parser):
            """
            test if args is number only

            args -- arguments to test
            args_name -- f'{args.EXEMPLE=}'.split("=")[0].split(".")[1] provide the name of args(var) for error message
            parser -- arg parser to print_help()

            return True if args is number only else exit and print help message
            """
            try:
                int(args)
            except ValueError:
                print("ERROR: {0} must be number only you have entered: {1}\r\n".format(args_name, args))
                parser.print_help()
                sys.exit(1)
            else:
                return True

if __name__ == "__main__":
    dev = py_hydro2mqtt()
    asyncio.run(dev.async_run())